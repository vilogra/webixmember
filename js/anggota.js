var anggota = {
  rows: [
    {
      view: "datatable",
      id: "clientTable",
      css: "datatable",
      data: data_anggota,
      columns: [
        {
          id: "id",
          header: [
            { text: "#" },
            { text: "<span class='webix-icon fa fa-filter'></span>" }
          ],
          width: 50,
          sort: "int"
        },
        {
          id: "fullname",
          header: [{ text: "Nama Anggota" }, { content: "textFilter" }],
          fillspace: true,
          sort: "string",
          editor: "text"
        },
        {
          id: "branch",
          header: [{ text: `Alamat` }],
          width: 100
        },
        {
          id: "level",
          header: [{ text: "Status Keanggotaan" }, { content: "selectFilter" }],
          width: 160,
          options: ["Premium", "Standard", "Not Active"],
          editor: "richselect"
        },
        {
          header: [
            {
              text: "Aksi"
            }
          ],
          template: "{common.trashIcon} {common.editIcon}",
          width: 100
        }
      ],
      borderless: true,
      yCount: 5,
      scroll: false,
      select: true,
      editable: true,
      editaction: "dblclick",
      type: {
        trashIcon: "<span class='webix-icon fa fa-trash-alt'></span>",
        editIcon: "<span class='webix-icon fa fa-pencil-alt'></span>"
      },
      pager: "pager"
    },
    {
      view: "pager",
      id: "pager",
      css: "pager",
      size: 5,
      template: "{common.prev()} {common.pages()} {common.next()}"
    }
  ]
};
