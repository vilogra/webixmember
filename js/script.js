var anggota = {
  id: "anggota",
  rows: [anggota]
};

var header = {
  css: "header",
  view: "toolbar",
  padding: 1,
  margin: 0,
  height: 60,
  cols: [
    {
      view: "label",
      type: "icon",
      icon: "fas fa-home",
      label: "<span class='fas fa-home'></span> Home"
    },
    {
      view: "label",
      label: "Keanggotaan"
    },
    {},
    {},
    {},
    {},
    {},
    {}
  ]
};

webix.ready(() => {
  webix.ui({
    rows: [
      header,
      {
        view: "form",
        width: 600,
        elements: [
          {
            view: "label",
            label: "Login"
          },
          {
            view: "text",
            label: "Email",
            type: "email",
            placeholder: "email@example.com"
          },
          {
            view: "text",
            label: "Password",
            type: "password"
          },
          {
            cols: [
              {
                view: "button",
                type: "icon",
                css: "webix_primary",
                icon: "wxi-pencil",
                label: "Login",
                click: () => {
                  alert(`Clicked`);
                }
              },
              {
                view: "button",
                type: "reset",
                value: "Forgot Password"
              }
            ]
          }
        ]
      },
      anggota
    ]
  });
});
